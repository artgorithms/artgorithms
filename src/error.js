window.ARGO = window.ARGO || {}

ARGO.ExecError = function (message, line, position, length) {
  this.message = message;
  this.line = line || null;
  this.position = position || null;
  this.length = length || null;
}

ARGO.ExecError.prototype = {
  'message': null,
  'category': "execution error",
  'line': null,
  'position': null,
  'length': null,
  'constructor': ARGO.ExecError
}

ARGO.CompError = function (message, line, position, length) {
  this.message = message;
  this.line = line || null;
  this.position = position || null;
  this.length = length || null;
}

ARGO.CompError.prototype = {
  'message': null,
  'category': "compilation error",
  'line': null,
  'position': null,
  'length': null,
  'constructor': ARGO.CompError
}

