window.ARGO = window.ARGO || {};

(function(){

  const BATCH_SIZE = 1000;
  const START_FUEL = 100000;

  var primitives = {
    SQUARE: ctx => {
      ctx.fillRect(-0.5, -0.5, 1.0, 1.0);
    },
    CIRCLE: ctx => {
      ctx.beginPath();
      ctx.arc(0, 0, 0.5, 0, 2 * Math.PI);
      ctx.fill();
    },
    TRIANGLE: ctx => {
      ctx.beginPath();
      ctx.moveTo(0.5, -0.28868);  // -sqrt(3)/6
      ctx.moveTo(0, 0.57735);  // 1/sqrt(3)
      ctx.moveTo(-0.5, -0.28868);  // -sqrt(3)/6
      ctx.fill();
    },
  }

  var initState = function(x, y) {
    if (x == null)
      x = ARGO.canvas.width / 2
    if (y == null)
      y = ARGO.canvas.height / 2
    if (Math.round(x * y) == 0)
      return null; // zero area
    var st = ARGO.Trans.identity({
      matrix: [10, 0, 0, -10, x, y],
    });
    st.fuel = START_FUEL;
    return st;
  }

  var sample = function(rules) {
    var r = _.random(0.0, 1.0, true)
    for (var idx = 0; idx < rules.length; idx++) {
      r -= rules[idx].weight
      if (r < 0.0) {
        return rules[idx]
      }
    }
  }

  var execLine = function(ir, st, line) {
    if (line.type == "call") {
      var newSt = ARGO.Trans.apply(st, line.transform);
      newSt.fuel = st.fuel - 1;
      var nm = line.name
      var prim = primitives[nm]
      if (prim) {
        ARGO.canvasCtx.setTransform.apply(ARGO.canvasCtx, newSt.matrix);
        ARGO.canvasCtx.fillStyle = "hsl(" + newSt.hue + ", " + (newSt.saturation * 100) + "%, " + (newSt.brightness * 100) + "%)";
        prim(ARGO.canvasCtx);
      } else {
        pushCall(ir, nm, newSt)
      }
    } else {
      throw new ARGO.ExecError("Unknown rule body line type: " + line.type);
    }
  }

  var runWorklist = function() {
     if (!ARGO.runLoopId) {
      ARGO.runLoopId = setInterval(function() {
        for (var i = 0; i < Math.min(BATCH_SIZE, ARGO.worklist.length); i++) {
          if (ARGO.worklist.length) {
            var args = ARGO.worklist.dequeue()
            call(args.ir, args.nm, args.st)
          } else {
            ARGO.stop()
          }
        }
      }, 0)
    }
  }

  var pushCall = function(ir, nm, st) {
    var scale = ARGO.Trans.getScale(st) * st.fuel;
    if (scale < 0) return;  // out of fuel or zero area
    ARGO.worklist.queue({
      ir: ir,
      nm: nm,
      st: st,
      scale: scale
    })
  }

  var call = function(ir, nm, st) {
    var defs = ir.rules[nm]

    if (!defs) {
      throw new ARGO.CompError("Unknown rule: " + nm)
    }

    var def = sample(defs)

    for (var i = 0; i < def.body.length; i++) {
      execLine(ir, st, def.body[i]);
    }

    runWorklist()
  }

  var interpret = function(ir) {
    if (!ir.startshape) {
      throw new ARGO.CompError("No startshape directive found!")
    }

    var st = initState()

    call(ir, ir.startshape, st)
  }

  var stop = function() {
    clearInterval(ARGO.runLoopId);
    ARGO.runLoopId = 0
    ARGO.worklist.clear()
  }

  ARGO.interpret = interpret
  ARGO.call = call
  ARGO.stop = stop
  ARGO.initState = initState
})()
