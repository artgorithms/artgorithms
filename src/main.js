window.ARGO = window.ARGO || {};

(function() {

  ARGO.clear = function() {
    ARGO.canvasCtx.setTransform(1, 0, 0, 1, 0, 0);
    ARGO.canvasCtx.clearRect(0, 0, ARGO.canvas.width, ARGO.canvas.height)
  }

  var updateRuleBtns = function(ir) {
    ARGO.ruleBtns.empty()
    _.forEach(ir.rules, function(def, nm) {
      if (_.startsWith(nm, '__')) {
        var btn = $("<button class='ruleBtn' onclick='ARGO.onRuleBtnClick(\""+nm+"\")'>"+_.trim(nm,'_')+"</button>")
        ARGO.ruleBtns.append(btn)
      }
    })
  }

  ARGO.onRuleBtnClick = function(nm) {
    ARGO.selectedRule = nm
  }

  ARGO.currentIR = null;

  ARGO.build = function() {
    // TODO: graphically display error in web IDE
    try {
      var txt = ARGO.aceEditor.getValue();
      //parse and translate
      var ast = ARGO.translate(ARGO.parse(txt));
      //compile
      var ir = ARGO.compile(ast);
      ARGO.currentIR = ir
      //create buttons      
      updateRuleBtns(ir)
    } catch (e) {
      window.alert(e.message);
    }
  }

  ARGO.buildAndRun = function() {
    // TODO: graphically display error in web IDE
    try {
      //build
      ARGO.build()
      //stop
      ARGO.stop();
      //clear
      ARGO.clear();
      //run
      var ir = ARGO.currentIR
      if (ir.startshape) {
        ARGO.interpret(ir);
      }
    } catch (e) {
      window.alert(e.message);
    }
  }

  var initEditor = function() {
    ARGO.jqEditor = $("#editor")[0]

    // init the editor    
    var aceEditor = ace.edit("editor");
    aceEditor.setTheme("ace/theme/monokai");
    aceEditor.getSession().setMode("ace/mode/lua");
    aceEditor.$blockScrolling = Infinity;
    ARGO.aceEditor = aceEditor

    // fill with default program
    var cachedTxt = localStorage.getItem("EDITOR")
    if (cachedTxt == "") {
      var astReq = $.get("tests/stem.argo")
        .done(txt => {
          localStorage.setItem("EDITOR", txt);
          aceEditor.setValue(txt)
          aceEditor.clearSelection()
        })
        .fail(() => { 
          throw new CompError("Failed to get file from server.")
        })
    } else {
      aceEditor.setValue(cachedTxt);
      aceEditor.clearSelection();
    }

    // persist the editor
    aceEditor.getSession().on('change', function(e) {
        localStorage.setItem("EDITOR", aceEditor.getValue());
    });
  }

  ARGO.main = function(){
    ARGO.canvas = $("#canvas")[0]
    ARGO.canvasCtx = ARGO.canvas.getContext('2d')
    ARGO.ruleBtns = $("#ruleBtns");
    
    initEditor()

    $(ARGO.canvas).click(function(e) {
      var x = e.offsetX
      var y = e.offsetY
      if (ARGO.selectedRule) {
        var st = ARGO.initState(x, y)
        var nm = ARGO.selectedRule;
        var ir = ARGO.currentIR;
        ARGO.call(ir, nm, st)
      }
    });

    //worklist setup
    ARGO.worklist = new PriorityQueue({ 
      comparator: function(a, b) { 
        return b.scale - a.scale; 
    }});
  }

  try {
    $(document).ready(ARGO.main)
  } catch (e) {
    console.log(e.message);
  }

})()
