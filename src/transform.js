window.ARGO = window.ARGO || {};

(function() {
  ARGO.Trans = {}
  function identity(o) {
    o = o || {};
    return {
      matrix: (o.matrix === undefined ? [1, 0, 0, 1, 0, 0] : o.matrix),
      hue: (o.hue === undefined ? 0 : o.hue),
      saturation: (o.saturation === undefined ? 1 : o.saturation),
      brightness: (o.brightness === undefined ? 1 : o.brightness),
    };
  }
  ARGO.Trans.identity = identity;
  
  function multiply(m, t) {
    return [
      m[0] * t[0] + m[2] * t[1],
      m[1] * t[0] + m[3] * t[1],
      m[0] * t[2] + m[2] * t[3],
      m[1] * t[2] + m[3] * t[3],
      m[0] * t[4] + m[2] * t[5] + m[4],
      m[1] * t[4] + m[3] * t[5] + m[5]
    ];
  }

  function clamp(x, a, b) {
    if (x < a) {
      return a;
    } else if (x > b) {
      return b;
    } else {
      return x;
    }
  }

  function mod(n, m) {
    return ((n % m) + m) % m;
  }

  ARGO.Trans.apply = function(orig, trans) {
    return {
      matrix: multiply(orig.matrix, trans.matrix),
      hue: mod(orig.hue + trans.hue, 360),
      saturation: clamp(orig.saturation * trans.saturation, 0, 1),
      brightness: clamp(orig.brightness * trans.brightness, 0, 1),
    };
  }

  ARGO.Trans.translate = function(dx, dy) {
    return identity({
      matrix: [1, 0, 0, 1, dx, dy],
    });
  }

  ARGO.Trans.scale = function(sx, sy) {
    return identity({
      matrix: [sx, 0, 0, sy, 0, 0],
    });
  }

  ARGO.Trans.getScale = function(st) {
    return Math.abs(st.matrix[0] * st.matrix[3] + st.matrix[1] * st.matrix[2]);
  }

  ARGO.Trans.rotate = function(rot) {
    var r = Math.PI * rot / 180;
    return identity({
      matrix: [ Math.cos(r), Math.sin(r), -Math.sin(r), Math.cos(r), 0, 0 ],
    });
  }

  ARGO.Trans.color = function(hue, saturation, brightness) {
    return identity({
      hue: hue,
      saturation: saturation,
      brightness: brightness,
    });
  }
})();
