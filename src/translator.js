(() => {
  window.ARGO = window.ARGO || {};

  var parameters = {
    'x-position': {
      'duplication': false,
      'type': "translation",
      'arguments': ['dx'],
      'default': { 'dy': 0 }
    },
    'y-position': {
      'duplication': false,
      'type': "translation",
      'arguments': ['dy'],
      'default': { 'dx': 0 }
    },
    'rotation': {
      'duplication': false,
      'type': "rotation",
      'arguments': ['r'],
      'default': {}
    },
    'hue': {
      'duplication': false,
      'type': "color",
      'arguments': ['h'],
      'default': { 'sat': 1, 'b': 1 }
    },
    'saturation': {
      'duplication': false,
      'type': "color",
      'arguments': ['sat'],
      'default': { 'h': 0, 'b': 1 }
    },
    'brightness': {
      'duplication': false,
      'type': "color",
      'arguments': ['b'],
      'default': { 'h': 0 , 'sat': 1 }
    },
    'scale': {
      'duplication': true,
      'type': "scale",
      'arguments': ['sx', 'sy'],
      'default': { 'sx': 1 , 'sy': 1 }
    }
  };
  parameters['x'] = parameters['x-position'];
  parameters['y'] = parameters['y-position'];
  parameters['r'] = parameters['rotation'];
  parameters['s'] = parameters['scale'];
  parameters['h'] = parameters['hue'];
  parameters['b'] = parameters['brightness'];
  parameters['sat'] = parameters['saturation'];

  /*
   * Convert a literal AST node for a rule parameter into an IR-appropriate
   * version.
   */
  function translateTransformation(transformation) {
    var description = parameters[transformation.parameter];
    if (!description) {
      throw new ARGO.CompError("Unknown transformation: "+transformation.parameter);
    }
    var duplicate = description.duplication && transformation.arguments.length == 1;
    if (transformation.arguments.length != description.arguments.length && !duplicate)
      throw new ARGO.CompError(
        "Incorrect number of arguments for parameter '{parameter}' at position {position} on line {line}"
        .supplant(transformation)
      );
    var translated = _.clone(description.default);
    translated.type = description.type;
    translated.line = transformation.line;
    translated.position = transformation.position;
    translated.length = transformation.length;
    for (var i = 0; i < description.arguments.length; ++i)
      translated[description.arguments[i]] = transformation.arguments[duplicate ? 0 : i];
    return translated;
  }

  /*
   * Abstract Syntax Tree Translator for Artgorithms language
   *
   * Converts literal rule invocation syntax to more intuitive form for the
   * intermediate representation compiler.
   */
  function translate(ast) {
    ast.forEach((directive) => {
      if (directive.type == "rule")
        directive.body.forEach(
          (invocation) =>  invocation.transform = invocation.transform.map(translateTransformation)
        );
      return;
    });
    return ast
  }
  ARGO.translate = translate;
})();
