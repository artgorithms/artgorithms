(() => {
  window.ARGO = window.ARGO || {};

  /*
   * Recursive Descent Parser for Artgorithms language
   *
   * parse{Nonterminal} - recognize a nonterminal production and return an AST
   * accept{Terminal} - recognize a terminal production and return it
   *
   * Note that we're considering identifiers to be terminal productions.
   */
  function parse(string) {
    var index = 0;

    /*
     * Parse Program -> (Rule|Startshape)*
     */
    function parseProgram(stream) {
      /*
       * Accept Ident -> [A-Za-z][A-Za-z0-9]*
       */
      function acceptIdent(stream) {
        var string = stream[index++].lexeme;
        if (!(/^[_A-Za-z][A-Za-z0-9_-]*$/.test(string)))
          throw new ARGO.CompError(
            ("Expected identifier at character {position} on line {line}" + 
             " but got {lexeme}").supplant(stream[index - 1]),
            stream[index - 1].line,
            stream[index - 1].position,
            stream[index - 1].length
          );
        return string;
      }
      /*
       * Parse Startshape -> 'startshape' Ident
       */
      function parseStartshape(stream) {
        return {
          'type':'startshape',
          'value': acceptIdent(stream)
        };
      }
      /*
       * Parse Rule -> 'rule' Ident Float? '{' RuleLine* '}'
       */
      function parseRule(stream) {
        /*
         * Accept '{'
         */
        function acceptLeftBrace(stream) {
          if (stream[index++].lexeme != '{')
            throw new ARGO.CompError(
              ("Expected left brace at character {position} on line {line}" +
               " but got {lexeme}").supplant(stream[index - 1]),
              stream[index - 1].line,
              stream[index - 1].position,
              stream[index - 1].length
            );
          return '{';
        }
        /*
         * Accept RuleLine -> Ident '{' (Ident Float)* '}'
         */
        function parseRuleLine(stream) {
          /*
           * Accept Float
           */
          function acceptFloat(stream) {
            var string = stream[index++].lexeme;
            var number = parseFloat(string);
            if (isNaN(number) && !(/^[+-]?([0-9]+|([0-9]*[.][0-9]+)?)$/.test(string)))
              throw new ARGO.CompError(
                ("Expected number at character {position} on line {line}" +
                 " but got {lexeme}").supplant(stream[index - 1]),
                stream[index - 1].line,
                stream[index - 1].position,
                stream[index - 1].length
              );
            return number;
          }
          var ast = {};
          ast.type = 'call';
          ast.name = acceptIdent(stream);
          acceptLeftBrace(stream);
          ast.transform = [];
          while (stream[index].lexeme != '}') {
            ast.transform.push({
              'parameter': acceptIdent(stream),
              'arguments': [],
              'line': stream[index].line,
              'position': stream[index].position,
              'length': stream[index].length
            });
            var tranformation = ast.transform[ast.transform.length - 1];
            while (/^[+-]?([0-9]+|([0-9]*[.][0-9]+)?)$/.test(stream[index].lexeme)) {
              tranformation.length += stream[index].length;
              tranformation.arguments.push(acceptFloat(stream));
            }
          }
          acceptRightBrace(stream);
          return ast;
        }
        /*
         * Accept '}'
         */
        function acceptRightBrace(stream) {
          if (stream[index++].lexeme != '}')
            throw new ARGO.CompError(
              ("Expected right brace at character {position} on line {line}" +
               " but got {lexeme}").supplant(stream[index - 1]),
              stream[index - 1].line,
              stream[index - 1].position,
              stream[index - 1].length
            );
          return '}';
        }
        var ast = {};
        ast.type = 'rule';
        ast.name = acceptIdent(stream);
        ast.probability = {};
        ast.probability.type = 'weight';
        ast.probability.line = stream[index].line;
        ast.probability.position = stream[index].position;
        var weight = parseFloat(stream[index].lexeme);
        if (!isNaN(weight)) {
          ast.probability.value = weight;
          ast.probability.length = stream[index].length;
          index++;
          if (stream[index].lexeme == "%") {
            ast.probability.type = 'percentage';
            ast.probability.value /= 100;
            ast.probability.length += stream[index].length;
            index++;
          }
        } else {
          ast.probability.value = 1;
          ast.probability.length = 0;
        }
        acceptLeftBrace(stream);
        ast.body = [];
        while (stream[index].lexeme != '}')
          ast.body.push(parseRuleLine(stream));
        acceptRightBrace(stream);
        return ast;
      }
      var ast = [];
      while (index < stream.length) {
        switch (stream[index++].lexeme) {
          case "startshape":
            ast.push(parseStartshape(stream));
            break;
          case "rule":
            ast.push(parseRule(stream));
            break;
          default:
            throw new ARGO.CompError(
              ("Expected either 'rule' or 'startshape' at position {position} on line {line}" +
               " but got '{lexeme}'").supplant(stream[index - 1]),
              stream[index - 1].line,
              stream[index - 1].position,
              stream[index - 1].length
            );
        }
      }
      return ast;
    }
    return parseProgram(lex(string));
  }
  ARGO.parse = parse;

  /*
   * Perform lexical analysis and return a list of objects in the following
   * form: {'line': #, 'position': #, 'length': #, 'lexeme': "..."}.
   */
  function lex(string) {
    // wow such computer science!
    var tokens = string.match(/[{}]|[_A-Za-z][A-Za-z0-9_-]*|#.*\n|\n|[\t ]+|[+-]?[0-9]*[.][0-9]+|[+-]?[0-9]+|%/gm);
    var line = 1;
    var position = 1;
    var lexemes = [];

    tokens.forEach((string) => {
      if (string == "\n" || string.charAt(0) == "#") {
        line++;
        position = 1;
      } else if (/[\t ]+/.test(string)) {
        position += string.length;
      } else if (string != "") {
        lexemes.push({
          'line': line,
          'position': position,
          'length': string.length,
          'lexeme': string
        });
      }
    });
    return lexemes;
  }
})();
