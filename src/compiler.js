window.ARGO = window.ARGO || {};

(function(){

  var compileTransformation = function(trans) {
    switch (trans.type) {
      case "translation":
        return ARGO.Trans.translate(trans.dx, trans.dy);
      case "scale":
        return ARGO.Trans.scale(trans.sx, trans.sy);
      case "rotation":
        return ARGO.Trans.rotate(trans.r);
      case "color":
        return ARGO.Trans.color(trans.h, trans.sat, trans.b);
      default:
        throw new ARGO.CompError("Unknown transformation type: " + trans.type);
    }
  }

  var compileLine = function(line) {
    if (line.type == "call") {
      var transform = ARGO.Trans.identity();
      _.forEach(line.transform, (transformation, index) => {
        transform = ARGO.Trans.apply(transform, compileTransformation(transformation));
      });
      return { type: "call", name: line.name, transform: transform }
    } else {
      throw new ARGO.CompError("Unknown rule body line type: " + line.type);
    }
    return null
  }

  var normalizeWeights = function(rules) {
    _.forEach(rules, (defs, ruleName) => {
      var percentageSum = 0.0;
      var weightSum = 0.0;
      _.forEach(defs, (def, index) => {
        if (def.probability.type === "percentage") {
          if (!(0 <= def.probability.value && def.probability.value <= 1)) {
            throw new ARGO.CompError("Rule percentage must be between 0% and 100%");
          }
          percentageSum += def.probability.value;
        } else if (def.probability.type == "weight") {
          if (def.probability.value < 0) {
            throw new ARGO.CompError("Rule weight must be greater than zero");  // or equal
          }
          weightSum += def.probability.value;
        } else {
          throw new ARGO.CompError("Unknown probability type: " + def.probability.type);
        }
      });
      if (percentageSum > 1) {
        throw new ARGO.CompError("Rule percentages exceed 100% for " + ruleName);
      }
      if (percentageSum === 1.0 && weightSum > 0.0) {
        throw new ARGO.CompError("Rule percentages sum to 100% so rules with weight will not get called");
      }
      if (percentageSum > 0.0 && weightSum === 0.0) {
        throw new ARGO.CompError("Rule percentages do not add up to 100%");
      }
      var verify = 0.0;
      _.forEach(defs, (def, index) => {
        if (def.probability.type === "percentage") {
          def.weight = def.probability.value;
        } else if (def.probability.type == "weight") {
          def.weight = (1 - percentageSum) * (def.probability.value / weightSum);
        } else {
          throw new Error();
        }
        verify += def.weight;
        delete def.probability;
      });
      if (Math.abs(1 - verify) >= 0.001) {
        throw new Error("Internal error: Probabilities do not add to 1.0 for " + ruleName + " (" + verify + ")");
      }
    });
  }

  var compileDirective = function(dir, ir) {
    if (dir.type == "startshape") {
      if (ir.startshape !== null) {
        throw new ARGO.CompError("Multiple startshape declarations: " + ir.startshape + " and " + dir.value);
      }
      ir.startshape = dir.value
    } else if (dir.type == "rule") {
      var nm = dir.name
      var defs = ir.rules[nm]
      if (defs === undefined) {
        defs = ir.rules[nm] = []
      }
      var body = _.map(dir.body, compileLine)
      defs.push({probability: dir.probability, body: body})
    } else {
      throw new ARGO.CompError("Unknown AST node: " + dir.type);
    }
  }

  var compile = function(ast) {
    var ir = {
      startshape: null,
      rules: {}
    }
    var probabilities = {};
    if (_.isArray(ast)) {
      ast.forEach(n => compileDirective(n, ir, probabilities));
    } else {
      compileDirective(ast, ir);
    }
    normalizeWeights(ir.rules);
    return ir;
  }

  ARGO.compile = compile

})()
