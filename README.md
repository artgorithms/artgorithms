Artgorithms
-----------

To run Artgorithms, run `./localhost.sh` in this directory and then go to
[localhost:4321](http://localhost:4321) in Chrome version 48 (or later, maybe).
The tests and demo directories contain code that can be run via the web IDE.

Relevant project documents:
 * [Design document](https://docs.google.com/document/d/1xQnwfwXcd2dbXytet7iwF0ajd1rK-rrFCB3ri1DZcyM/edit)
 * [Project proposal](https://docs.google.com/document/d/1PW-6Q6_n-_SK-LeTAKmDBE07eNRcdFyv1oRdYK2trJA/edit#heading=h.zheel11bgy2m)
 * [Poster slides](https://docs.google.com/presentation/d/1qsJu4ODx2o9y8oN00lhxd9vd1SEZgNlDhdbvmF0tg2g/edit#slide=id.g11eb4cef8a_0_0)

The most recent versions of each of these documents are included in this
repository; however, you may wish to refer to the online version to see the
editing history. The project proposal includes the tutorial and references.
